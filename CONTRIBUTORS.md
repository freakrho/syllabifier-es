# Contributors

The following people have contributed to the development of Syllabifier ES. If you submit a pull request, please add your name to the list below.

* 2019-ongoing: Rodrigo Pérez Di Matteo <rodrigo@pomelogames.com>
