# Syllabifier ES

Syllabifier ES is tool for counting the syllables in a word in spanish.
It implements Heriberto Cuayáhuitl's **Syllabification Algorithm** which can be found here:
[A Syllabification Algorithm for Spanish (2004)](http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.61.9215)

## Usage
Copy the folder `syllabifier_es` to your project and import it like this:
```python
from syllabifier_es.syllabifier import Syllabifier

if __name__ == "__main__":
    print("Hola has " + str(Syllabifier.scan_word("Hola")) + " syllables")
```
