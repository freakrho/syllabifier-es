class SyllabifierData:
    DATA = {
      "vowels": {
        "strong": "aeo",
        "weak": "iu",
        "strong_accent": "áéó",
        "weak_accent": "íú"
      },
      "consonants":{
        "singles": "bcdfghjklmñnpqrstvwxyz",
        "doubles": [
          "ll", "rr", "ch", "qu"
        ]
      },
      "prefixes": [
        "circun", "cuadri", "cuadru", "cuatri",
        "quinqu", "archi", "arqui", "citer", "cuasi", "infra", "inter", "intra", "multi", "radio", "retro", "satis",
        "sobre", "super", "supra", "trans", "ulter", "ultra", "yuxta", "ante", "anti", "cata", "deci", "ecto", "endo",
        "hemi", "hipo", "meta", "omni", "pali", "para", "peri", "post", "radi", "tras", "vice", "cons", "abs", "ana",
        "apo", "arz", "bis", "biz", "cis", "com", "con", "des", "dia", "dis", "dis", "epi", "exo", "met", "pen", "pos", "pre",
        "pro", "pro", "tri", "uni", "viz", "ins", "nos"
      ]
    }
