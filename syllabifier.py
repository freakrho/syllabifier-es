from .syllabifier_data import SyllabifierData
from enum import Enum


# vowel categories
class VowelType(Enum):
    STRONG = 1
    WEAK = 2
    STRONG_ACCENT = 3
    WEAK_ACCENT = 4
    NONE = 5


class Syllabifier:
    @classmethod
    def is_vowel(cls, char: str):
        for vowels in SyllabifierData.DATA["vowels"]:
            if char in SyllabifierData.DATA["vowels"][vowels]:
                return True
        return False

    @classmethod
    def is_strong_vowel(cls, char: str):
        for vowel in SyllabifierData.DATA["vowels"]["strong"]:
            if char == vowel:
                return True
        for vowel in SyllabifierData.DATA["vowels"]["strong_accent"]:
            if char == vowel:
                return True
        return False

    @classmethod
    def is_double_consonant(cls, pair: str):
        for double in SyllabifierData.DATA["consonants"]["doubles"]:
            if pair == double:
                return True
        return False

    @classmethod
    def get_consecutive_consonants(cls, string: str):
        length = 0
        while length < len(string):
            if not cls.is_vowel(string[length]):
                length += 1
            else:
                return length
        return length

    @classmethod
    def get_consecutive_vowels(cls, string: str):
        length = 0
        while length < len(string):
            if cls.is_vowel(string[length]):
                length += 1
            else:
                return length
        return length

    @classmethod
    def replace_double_consonants(cls, word: str):
        result = word
        for double in SyllabifierData.DATA["consonants"]["doubles"]:
            result = result.replace(double, "X")
        return result

    @classmethod
    def vowel_category(cls, char: str):
        """
        Returns the category a vowel belongs to: strong, weak, strong_accent or weak_accent
        :param char: str
        :return: VowelType
        """
        for vowel in SyllabifierData.DATA["vowels"]["strong"]:
            if char == vowel:
                return VowelType.STRONG
        for vowel in SyllabifierData.DATA["vowels"]["strong_accent"]:
            if char == vowel:
                return VowelType.STRONG_ACCENT
        for vowel in SyllabifierData.DATA["vowels"]["weak"]:
            if char == vowel:
                return VowelType.WEAK
        for vowel in SyllabifierData.DATA["vowels"]["weak_accent"]:
            if char == vowel:
                return VowelType.WEAK_ACCENT
        return VowelType.NONE

    @classmethod
    def scan_word(cls, word: str):
        """
        Scan a word in spanish for syllables
        :param word: str
        :return: int
        """
        # remove spaces (assume they are user errors)
        word.replace(" ", "")
        # use the word in lower case to avoid trouble
        word = word.lower()

        if len(word) == 1:
            return 1

        divisions = []
        index = 0
        # if it starts with a prefix, add a division
        for pref in SyllabifierData.DATA["prefixes"]:
            if word.startswith(pref):
                divisions.append(len(pref))
                index = len(pref)
                # print("Prefix [" + pref + "] found")
                break

        # replace double consonants so they are treated as one letter
        word = cls.replace_double_consonants(word)
        # print("After replacing doubles: " + word)
        if len(word) == 0:  # word was actually a symbol
            return 0

        # if it starts with one or two consonants, ignore them
        if index == 0 and not cls.is_vowel(word[0]):
            index += 1
            if not cls.is_vowel(word[1]):
                index += 1

        # scan word for consonants from left to right
        for i in range(index, len(word)):
            # if consonant
            if not cls.is_vowel(word[i]):
                # if previous is constant, we already checked it
                if i == 0 or cls.is_vowel(word[i - 1]):
                    consonants_length = cls.get_consecutive_consonants(word[i + 1:]) + 1

                    if i + consonants_length < len(word):
                        last_consonant = word[i + consonants_length - 1]
                        if last_consonant == "l" or last_consonant == "r":
                            divisions.append(i + consonants_length - 1)
                        else:
                            divisions.append(i + consonants_length - 2)
                    else:
                        break

        # scan the word for vowels
        for i in range(index, len(word)):
            # if vowel
            if cls.is_vowel(word[i]):
                # if previous is vowel, we already checked it
                if i == 0 or cls.is_vowel(word[i - 1]):
                    vowels_length = cls.get_consecutive_vowels(word[i + 1:]) + 1
                    if vowels_length == 2:
                        vowel1 = cls.vowel_category(word[i])
                        vowel2 = cls.vowel_category(word[i + 1])
                        if vowel1 == VowelType.STRONG_ACCENT or vowel2 == VowelType.STRONG_ACCENT:
                            divisions.append(i + 1)
                        elif vowel1 == VowelType.STRONG and vowel2 == VowelType.STRONG:
                            divisions.append(i + 1)
                    if vowels_length == 3:
                        for ind in range(i, vowels_length - 1):
                            if cls.is_strong_vowel(word[ind]) and cls.is_strong_vowel(word[ind + 1]):
                                divisions.append(ind)

        # text = ""
        # last = 0
        # for d in divisions:
        #     text += " " * (d - last)
        #     last = d
        #     text += "|"
        # print(text)
        # print(word)

        return len(divisions) + 1
